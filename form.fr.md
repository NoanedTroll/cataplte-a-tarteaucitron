---
title: 'Cyber entatartage'
template: form
form:
    name: CyberEntatartage
    classes: 'well col-md-12'
    fields:
        -
            name: twitteruser
            label: 'compte twitter'
            placeholder: 'le compte twitter de la victime sans le @'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            classes: 'form-control form-group'
            validate:
                required: true
        -
            name: serviceutiliser
            label: 'Service utiliser'
            help: 'Service utiliser par la personne (et oui j''ai pas fait de scanner js ...)'
            type: checkboxes
            classes: 'form-control form-group'
            options:
                googleapis: 'Google Api'
                googlemaps: 'Google Maps'
                googletagmanager: 'Google Tag Manager'
                googleanalytics: 'Google Analytics'
                TimelineJS: 'Timeline JS'
                Typekit: 'Typekit (Adobe)'
                Disqus: Disqus
                Facebook: 'Facebook (commentaire)'
                Twitter: Twitter
                Youtube: youtube
        -
            name: website
            type: honeypot
        -
            name: captcha
            label: 'Combien font deux fois 21 ?'
            placeholder: 'Cette question a pour but de vérifier que vous êtes bien un humain'
            type: text
            classes: 'form-control form-group'
            validate:
                pattern: '42'
                required: true
    buttons:
        -
            type: submit
            value: 'Envoyer dans la gueule'
            classes: 'btn btn-primary text-uppercase'
    process:
        -
            message: '<a href="https://twitter.com/intent/tweet?text=Et une %23tarteAuCitron dans la gueule de %40{{ form.value.twitteruser }} pour {% for key, value in form.value.serviceutiliser %}%40{{ value }}, {% endfor %}(c est du préventif pour la %23RGPD) %23cyberEntartage https://www.cnil.fr/fr/solutions-centralisees-de-recueil-de-consentement-aux-cookies-les-gestionnaires-de-tag merci la %40CNIL et %40tarteaucitronjs !">votre lien pour cyber entarter {{ form.value.twitteruser }}</a>'
        -
            display: lien-cyberentatage
---

Avant de faire un cyber-entartage pensée  à faire un tour sur [web archive pour sauvegarger une image du site au moment ou vous l'entarté](https://web.archive.org/save/)